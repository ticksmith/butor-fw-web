exports.defineTags = function(dictionary) {
	dictionary.defineTag("singleton", {
		mustNotHaveValue: true,
		onTagged: function(doclet, tag) {
			doclet.singleton = true;
		}
	});
};
