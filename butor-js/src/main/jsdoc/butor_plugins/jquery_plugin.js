var jquery_plugins_doclets = [];

exports.defineTags = function(dictionary) {
	dictionary.defineTag("jquery_plugin", {
		mustNotHaveValue: false,
		onTagged: function(doclet, tag) {
			doclet.jquery_plugin = true;

			switch (tag.value) {
				case 'mustBeInitilized':
					doclet.mustBeInitilized = true;
					break;
				default:
					doclet.mustBeInitilized = false;
			}

			jquery_plugins_doclets.push(doclet.name);
		}
	});
};

exports.handlers = {
	newDoclet: function(e) {
		if (e.doclet.kind === 'function' && e.doclet.memberof) {
			for (var i = 0; i < jquery_plugins_doclets.length; i++) {
				if (e.doclet.memberof === jquery_plugins_doclets[i]) {
					e.doclet.jquery_method_of = jquery_plugins_doclets[i];
				}
			}
		}
	}
};
