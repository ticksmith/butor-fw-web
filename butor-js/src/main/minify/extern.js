var jQuery = {
	"fn": function() {},
	"extend": function() {},
	"noConflict": function() {},
	"isReady": function() {},
	"readyWait": function() {},
	"holdReady": function() {},
	"ready": function() {},
	"isFunction": function() {},
	"isArray": function() {},
	"isWindow": function() {},
	"isNumeric": function() {},
	"type": function() {},
	"isPlainObject": function() {},
	"isEmptyObject": function() {},
	"error": function() {},
	"parseHTML": function() {},
	"parseJSON": function() {},
	"parseXML": function() {},
	"noop": function() {},
	"globalEval": function() {},
	"camelCase": function() {},
	"nodeName": function() {},
	"each": function() {},
	"trim": function() {},
	"makeArray": function() {},
	"inArray": function() {},
	"merge": function() {},
	"grep": function() {},
	"map": function() {},
	"guid": function() {},
	"proxy": function() {},
	"access": function() {},
	"now": function() {},
	"Callbacks": function() {},
	"Deferred": function() {},
	"when": function() {},
	"support": {
		"getSetAttribute": function() {},
		"leadingWhitespace": function() {},
		"tbody": function() {},
		"htmlSerialize": function() {},
		"style": function() {},
		"hrefNormalized": function() {},
		"opacity": function() {},
		"cssFloat": function() {},
		"checkOn": function() {},
		"optSelected": function() {},
		"enctype": function() {},
		"html5Clone": function() {},
		"boxModel": function() {},
		"deleteExpando": function() {},
		"noCloneEvent": function() {},
		"inlineBlockNeedsLayout": function() {},
		"shrinkWrapBlocks": function() {},
		"reliableMarginRight": function() {},
		"boxSizingReliable": function() {},
		"pixelPosition": function() {},
		"noCloneChecked": function() {},
		"optDisabled": function() {},
		"input": function() {},
		"radioValue": function() {},
		"appendChecked": function() {},
		"checkClone": function() {},
		"submitBubbles": function() {},
		"changeBubbles": function() {},
		"focusinBubbles": function() {},
		"clearCloneStyle": function() {},
		"cors": function() {},
		"ajax": function() {},
		"reliableHiddenOffsets": function() {},
		"boxSizing": function() {},
		"doesNotIncludeMarginInBodyOffset": function() {},
		"selectstart": function() {},
		"offsetFractions": function() {},
		"transition": {
			"end": function() {}
		}
	},
	"cache": {
		"2": {
			"events": {
				"mouseup": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				},
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"1": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"2": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"3": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"4": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"5": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"6": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"7": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"8": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				},
				"keydown": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				},
				"focusin": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"4": {
			"events": {
				"mouseout": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				},
				"mouseover": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"30": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"32": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"36": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"38": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"40": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"42": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"43": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"45": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"47": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"49": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"58": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"62": {
			"olddisplay": function() {}
		},
		"63": {
			"olddisplay": function() {}
		},
		"64": {
			"olddisplay": function() {}
		},
		"65": {
			"olddisplay": function() {}
		},
		"66": {
			"olddisplay": function() {}
		},
		"67": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"selector": function() {},
						"needsContext": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"70": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		},
		"74": {
			"olddisplay": function() {}
		},
		"75": {
			"events": {
				"click": {
					"0": {
						"type": function() {},
						"origType": function() {},
						"handler": function() {},
						"guid": function() {},
						"namespace": function() {}
					},
					"delegateCount": function() {}
				}
			},
			"handle": function() {}
		}
	},
	"expando": function() {},
	"noData": {
		"embed": function() {},
		"object": function() {},
		"applet": function() {}
	},
	"hasData": function() {},
	"data": function() {},
	"removeData": function() {},
	"_data": function() {},
	"_removeData": function() {},
	"acceptData": function() {},
	"queue": function() {},
	"dequeue": function() {},
	"_queueHooks": function() {},
	"valHooks": {
		"option": {
			"get": function() {}
		},
		"select": {
			"get": function() {},
			"set": function() {}
		},
		"radio": {
			"set": function() {}
		},
		"checkbox": {
			"set": function() {}
		}
	},
	"attr": function() {},
	"removeAttr": function() {},
	"attrHooks": {
		"type": {
			"set": function() {}
		},
		"value": {
			"get": function() {},
			"set": function() {}
		}
	},
	"propFix": {
		"tabindex": function() {},
		"readonly": function() {},
		"for": function() {},
		"class": function() {},
		"maxlength": function() {},
		"cellspacing": function() {},
		"cellpadding": function() {},
		"rowspan": function() {},
		"colspan": function() {},
		"usemap": function() {},
		"frameborder": function() {},
		"contenteditable": function() {}
	},
	"prop": function() {},
	"propHooks": {
		"tabIndex": {
			"get": function() {}
		}
	},
	"event": {
		"global": {
			"mouseup": function() {},
			"mouseout": function() {},
			"mouseover": function() {},
			"click": function() {},
			"load": function() {},
			"keydown": function() {},
			"focusin": function() {},
			"scroll": function() {},
			"hashchange": function() {},
			"resize": function() {}
		},
		"add": function() {},
		"remove": function() {},
		"trigger": function() {},
		"dispatch": function() {},
		"handlers": function() {},
		"fix": function() {},
		"props": {
			"0": function() {},
			"1": function() {},
			"2": function() {},
			"3": function() {},
			"4": function() {},
			"5": function() {},
			"6": function() {},
			"7": function() {},
			"8": function() {},
			"9": function() {},
			"10": function() {},
			"11": function() {},
			"12": function() {},
			"13": function() {},
			"14": function() {},
			"15": function() {},
			"16": function() {}
		},
		"fixHooks": {
			"touchcancel": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {}
				},
				"filter": function() {}
			},
			"touchend": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {}
				},
				"filter": function() {}
			},
			"touchmove": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {}
				},
				"filter": function() {}
			},
			"touchstart": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {}
				},
				"filter": function() {}
			},
			"focus": {
			},
			"mouseup": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			},
			"click": {
				"props": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"filter": function() {}
			}
		},
		"keyHooks": {
			"props": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {}
			},
			"filter": function() {}
		},
		"mouseHooks": {
			"props": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {},
				"4": function() {},
				"5": function() {},
				"6": function() {},
				"7": function() {},
				"8": function() {},
				"9": function() {},
				"10": function() {},
				"11": function() {}
			},
			"filter": function() {}
		},
		"special": {
			"load": {
				"noBubble": function() {}
			},
			"click": {
				"trigger": function() {}
			},
			"focus": {
				"trigger": function() {},
				"delegateType": function() {}
			},
			"blur": {
				"trigger": function() {},
				"delegateType": function() {}
			},
			"beforeunload": {
				"postDispatch": function() {}
			},
			"mouseenter": {
				"delegateType": function() {},
				"bindType": function() {},
				"handle": function() {}
			},
			"mouseleave": {
				"delegateType": function() {},
				"bindType": function() {},
				"handle": function() {}
			},
			"focusin": {
				"setup": function() {},
				"teardown": function() {}
			},
			"focusout": {
				"setup": function() {},
				"teardown": function() {}
			},
			"ajaxStart": {
				"setup": function() {},
				"teardown": function() {}
			},
			"ajaxStop": {
				"setup": function() {},
				"teardown": function() {}
			},
			"ajaxSend": {
				"setup": function() {},
				"teardown": function() {}
			},
			"ajaxComplete": {
				"setup": function() {},
				"teardown": function() {}
			},
			"ajaxError": {
				"setup": function() {},
				"teardown": function() {}
			},
			"ajaxSuccess": {
				"setup": function() {},
				"teardown": function() {}
			},
			"bsTransitionEnd": {
				"bindType": function() {},
				"delegateType": function() {},
				"handle": function() {}
			},
			"hashchange": {
				"add": function() {},
				"setup": function() {},
				"teardown": function() {}
			},
			"drag": {
				"defaults": {
					"which": function() {},
					"distance": function() {},
					"not": function() {},
					"relative": function() {},
					"drop": function() {},
					"click": function() {}
				},
				"datakey": function() {},
				"noBubble": function() {},
				"add": function() {},
				"remove": function() {},
				"setup": function() {},
				"teardown": function() {},
				"init": function() {},
				"interaction": function() {},
				"handler": function() {},
				"hijack": function() {},
				"properties": function() {},
				"element": function() {},
				"flatten": function() {},
				"textselect": function() {},
				"dontstart": function() {},
				"callback": function() {}
			},
			"dragend": {
				"defaults": {
					"which": function() {},
					"distance": function() {},
					"not": function() {},
					"relative": function() {},
					"drop": function() {},
					"click": function() {}
				},
				"datakey": function() {},
				"noBubble": function() {},
				"add": function() {},
				"remove": function() {},
				"setup": function() {},
				"teardown": function() {},
				"init": function() {},
				"interaction": function() {},
				"handler": function() {},
				"hijack": function() {},
				"properties": function() {},
				"element": function() {},
				"flatten": function() {},
				"textselect": function() {},
				"dontstart": function() {},
				"callback": function() {}
			},
			"dragstart": {
				"defaults": {
					"which": function() {},
					"distance": function() {},
					"not": function() {},
					"relative": function() {},
					"drop": function() {},
					"click": function() {}
				},
				"datakey": function() {},
				"noBubble": function() {},
				"add": function() {},
				"remove": function() {},
				"setup": function() {},
				"teardown": function() {},
				"init": function() {},
				"interaction": function() {},
				"handler": function() {},
				"hijack": function() {},
				"properties": function() {},
				"element": function() {},
				"flatten": function() {},
				"textselect": function() {},
				"dontstart": function() {},
				"callback": function() {}
			},
			"draginit": {
				"defaults": {
					"which": function() {},
					"distance": function() {},
					"not": function() {},
					"relative": function() {},
					"drop": function() {},
					"click": function() {}
				},
				"datakey": function() {},
				"noBubble": function() {},
				"add": function() {},
				"remove": function() {},
				"setup": function() {},
				"teardown": function() {},
				"init": function() {},
				"interaction": function() {},
				"handler": function() {},
				"hijack": function() {},
				"properties": function() {},
				"element": function() {},
				"flatten": function() {},
				"textselect": function() {},
				"dontstart": function() {},
				"callback": function() {}
			}
		},
		"simulate": function() {},
		"handle": function() {}
	},
	"removeEvent": function() {},
	"Event": function() {},
	"find": function() {},
	"expr": {
		"cacheLength": function() {},
		"createPseudo": function() {},
		"match": {
			"ID": {
			},
			"CLASS": {
			},
			"NAME": {
			},
			"TAG": {
			},
			"ATTR": {
			},
			"PSEUDO": {
			},
			"CHILD": {
			},
			"needsContext": {
			}
		},
		"find": {
			"ID": function() {},
			"TAG": function() {},
			"NAME": function() {},
			"CLASS": function() {}
		},
		"relative": {
			">": {
				"dir": function() {},
				"first": function() {}
			},
			" ": {
				"dir": function() {}
			},
			"+": {
				"dir": function() {},
				"first": function() {}
			},
			"~": {
				"dir": function() {}
			}
		},
		"preFilter": {
			"ATTR": function() {},
			"CHILD": function() {},
			"PSEUDO": function() {}
		},
		"filter": {
			"TAG": function() {},
			"CLASS": function() {},
			"ATTR": function() {},
			"CHILD": function() {},
			"PSEUDO": function() {},
			"ID": function() {}
		},
		"pseudos": {
			"not": function() {},
			"has": function() {},
			"contains": function() {},
			"lang": function() {},
			"target": function() {},
			"root": function() {},
			"focus": function() {},
			"enabled": function() {},
			"disabled": function() {},
			"checked": function() {},
			"selected": function() {},
			"empty": function() {},
			"parent": function() {},
			"header": function() {},
			"input": function() {},
			"button": function() {},
			"text": function() {},
			"first": function() {},
			"last": function() {},
			"eq": function() {},
			"even": function() {},
			"odd": function() {},
			"lt": function() {},
			"gt": function() {},
			"radio": function() {},
			"checkbox": function() {},
			"file": function() {},
			"password": function() {},
			"image": function() {},
			"submit": function() {},
			"reset": function() {},
			"nth": function() {},
			"hidden": function() {},
			"visible": function() {},
			"animated": function() {},
			"data": function() {},
			"focusable": function() {},
			"tabbable": function() {},
			"ui-mouse": function() {},
			"ui-draggable": function() {},
			"ui-droppable": function() {},
			"ui-resizable": function() {},
			"ui-selectable": function() {},
			"ui-sortable": function() {},
			"ui-accordion": function() {},
			"ui-autocomplete": function() {},
			"ui-button": function() {},
			"ui-buttonset": function() {},
			"ui-dialog": function() {},
			"ui-menu": function() {},
			"ui-progressbar": function() {},
			"ui-slider": function() {},
			"ui-spinner": function() {},
			"ui-tabs": function() {},
			"ui-tooltip": function() {}
		},
		"filters": {
			"not": function() {},
			"has": function() {},
			"contains": function() {},
			"lang": function() {},
			"target": function() {},
			"root": function() {},
			"focus": function() {},
			"enabled": function() {},
			"disabled": function() {},
			"checked": function() {},
			"selected": function() {},
			"empty": function() {},
			"parent": function() {},
			"header": function() {},
			"input": function() {},
			"button": function() {},
			"text": function() {},
			"first": function() {},
			"last": function() {},
			"eq": function() {},
			"even": function() {},
			"odd": function() {},
			"lt": function() {},
			"gt": function() {},
			"radio": function() {},
			"checkbox": function() {},
			"file": function() {},
			"password": function() {},
			"image": function() {},
			"submit": function() {},
			"reset": function() {},
			"nth": function() {},
			"hidden": function() {},
			"visible": function() {},
			"animated": function() {},
			"data": function() {},
			"focusable": function() {},
			"tabbable": function() {},
			"ui-mouse": function() {},
			"ui-draggable": function() {},
			"ui-droppable": function() {},
			"ui-resizable": function() {},
			"ui-selectable": function() {},
			"ui-sortable": function() {},
			"ui-accordion": function() {},
			"ui-autocomplete": function() {},
			"ui-button": function() {},
			"ui-buttonset": function() {},
			"ui-dialog": function() {},
			"ui-menu": function() {},
			"ui-progressbar": function() {},
			"ui-slider": function() {},
			"ui-spinner": function() {},
			"ui-tabs": function() {},
			"ui-tooltip": function() {}
		},
		"setFilters": {
			"not": function() {},
			"has": function() {},
			"contains": function() {},
			"lang": function() {},
			"target": function() {},
			"root": function() {},
			"focus": function() {},
			"enabled": function() {},
			"disabled": function() {},
			"checked": function() {},
			"selected": function() {},
			"empty": function() {},
			"parent": function() {},
			"header": function() {},
			"input": function() {},
			"button": function() {},
			"text": function() {},
			"first": function() {},
			"last": function() {},
			"eq": function() {},
			"even": function() {},
			"odd": function() {},
			"lt": function() {},
			"gt": function() {},
			"radio": function() {},
			"checkbox": function() {},
			"file": function() {},
			"password": function() {},
			"image": function() {},
			"submit": function() {},
			"reset": function() {},
			"nth": function() {},
			"hidden": function() {},
			"visible": function() {},
			"animated": function() {},
			"data": function() {},
			"focusable": function() {},
			"tabbable": function() {},
			"ui-mouse": function() {},
			"ui-draggable": function() {},
			"ui-droppable": function() {},
			"ui-resizable": function() {},
			"ui-selectable": function() {},
			"ui-sortable": function() {},
			"ui-accordion": function() {},
			"ui-autocomplete": function() {},
			"ui-button": function() {},
			"ui-buttonset": function() {},
			"ui-dialog": function() {},
			"ui-menu": function() {},
			"ui-progressbar": function() {},
			"ui-slider": function() {},
			"ui-spinner": function() {},
			"ui-tabs": function() {},
			"ui-tooltip": function() {}
		},
		"attrHandle": {
		},
		":": {
			"not": function() {},
			"has": function() {},
			"contains": function() {},
			"lang": function() {},
			"target": function() {},
			"root": function() {},
			"focus": function() {},
			"enabled": function() {},
			"disabled": function() {},
			"checked": function() {},
			"selected": function() {},
			"empty": function() {},
			"parent": function() {},
			"header": function() {},
			"input": function() {},
			"button": function() {},
			"text": function() {},
			"first": function() {},
			"last": function() {},
			"eq": function() {},
			"even": function() {},
			"odd": function() {},
			"lt": function() {},
			"gt": function() {},
			"radio": function() {},
			"checkbox": function() {},
			"file": function() {},
			"password": function() {},
			"image": function() {},
			"submit": function() {},
			"reset": function() {},
			"nth": function() {},
			"hidden": function() {},
			"visible": function() {},
			"animated": function() {},
			"data": function() {},
			"focusable": function() {},
			"tabbable": function() {},
			"ui-mouse": function() {},
			"ui-draggable": function() {},
			"ui-droppable": function() {},
			"ui-resizable": function() {},
			"ui-selectable": function() {},
			"ui-sortable": function() {},
			"ui-accordion": function() {},
			"ui-autocomplete": function() {},
			"ui-button": function() {},
			"ui-buttonset": function() {},
			"ui-dialog": function() {},
			"ui-menu": function() {},
			"ui-progressbar": function() {},
			"ui-slider": function() {},
			"ui-spinner": function() {},
			"ui-tabs": function() {},
			"ui-tooltip": function() {}
		}
	},
	"unique": function() {},
	"text": function() {},
	"isXMLDoc": function() {},
	"contains": function() {},
	"filter": function() {},
	"dir": function() {},
	"sibling": function() {},
	"clone": function() {},
	"buildFragment": function() {},
	"cleanData": function() {},
	"cssHooks": {
		"opacity": {
			"get": function() {}
		},
		"height": {
			"get": function() {},
			"set": function() {}
		},
		"width": {
			"get": function() {},
			"set": function() {}
		},
		"margin": {
			"expand": function() {}
		},
		"padding": {
			"expand": function() {},
			"set": function() {}
		},
		"borderWidth": {
			"expand": function() {},
			"set": function() {}
		},
		"backgroundColor": {
			"set": function() {}
		},
		"borderBottomColor": {
			"set": function() {}
		},
		"borderLeftColor": {
			"set": function() {}
		},
		"borderRightColor": {
			"set": function() {}
		},
		"borderTopColor": {
			"set": function() {}
		},
		"color": {
			"set": function() {}
		},
		"columnRuleColor": {
			"set": function() {}
		},
		"outlineColor": {
			"set": function() {}
		},
		"textDecorationColor": {
			"set": function() {}
		},
		"textEmphasisColor": {
			"set": function() {}
		},
		"borderColor": {
			"expand": function() {}
		}
	},
	"cssNumber": {
		"columnCount": function() {},
		"fillOpacity": function() {},
		"fontWeight": function() {},
		"lineHeight": function() {},
		"opacity": function() {},
		"orphans": function() {},
		"widows": function() {},
		"zIndex": function() {},
		"zoom": function() {}
	},
	"cssProps": {
		"float": function() {},
		"width": function() {},
		"boxSizing": function() {},
		"paddingRight": function() {},
		"borderRightWidth": function() {},
		"paddingLeft": function() {},
		"borderLeftWidth": function() {},
		"position": function() {},
		"height": function() {},
		"paddingTop": function() {},
		"borderTopWidth": function() {},
		"paddingBottom": function() {},
		"borderBottomWidth": function() {},
		"display": function() {},
		"top": function() {},
		"left": function() {},
		"cursor": function() {},
		"marginRight": function() {}
	},
	"style": function() {},
	"css": function() {},
	"swap": function() {},
	"param": function() {},
	"get": function() {},
	"post": function() {},
	"active": function() {},
	"lastModified": {
	},
	"etag": {
	},
	"ajaxSettings": {
		"url": function() {},
		"type": function() {},
		"isLocal": function() {},
		"global": function() {},
		"processData": function() {},
		"async": function() {},
		"contentType": function() {},
		"accepts": {
			"*": function() {},
			"text": function() {},
			"html": function() {},
			"xml": function() {},
			"json": function() {},
			"script": function() {}
		},
		"contents": {
			"xml": {
			},
			"html": {
			},
			"json": {
			},
			"script": {
			}
		},
		"responseFields": {
			"xml": function() {},
			"text": function() {}
		},
		"converters": {
			"* text": function() {},
			"text html": function() {},
			"text json": function() {},
			"text xml": function() {},
			"text script": function() {}
		},
		"flatOptions": {
			"url": function() {},
			"context": function() {}
		},
		"jsonp": function() {},
		"jsonpCallback": function() {},
		"xhr": function() {},
		"cache": function() {}
	},
	"ajaxSetup": function() {},
	"ajaxPrefilter": function() {},
	"ajaxTransport": function() {},
	"ajax": function() {},
	"getScript": function() {},
	"getJSON": function() {},
	"Animation": function() {},
	"Tween": function() {},
	"speed": function() {},
	"easing": {
		"linear": function() {},
		"swing": function() {},
		"easeInQuad": function() {},
		"easeOutQuad": function() {},
		"easeInOutQuad": function() {},
		"easeInCubic": function() {},
		"easeOutCubic": function() {},
		"easeInOutCubic": function() {},
		"easeInQuart": function() {},
		"easeOutQuart": function() {},
		"easeInOutQuart": function() {},
		"easeInQuint": function() {},
		"easeOutQuint": function() {},
		"easeInOutQuint": function() {},
		"easeInExpo": function() {},
		"easeOutExpo": function() {},
		"easeInOutExpo": function() {},
		"easeInSine": function() {},
		"easeOutSine": function() {},
		"easeInOutSine": function() {},
		"easeInCirc": function() {},
		"easeOutCirc": function() {},
		"easeInOutCirc": function() {},
		"easeInElastic": function() {},
		"easeOutElastic": function() {},
		"easeInOutElastic": function() {},
		"easeInBack": function() {},
		"easeOutBack": function() {},
		"easeInOutBack": function() {},
		"easeInBounce": function() {},
		"easeOutBounce": function() {},
		"easeInOutBounce": function() {}
	},
	"timers": {
	},
	"fx": function() {},
	"offset": {
		"setOffset": function() {}
	},
	"migrateMute": function() {},
	"migrateWarnings": {
		"0": function() {},
		"1": function() {}
	},
	"migrateTrace": function() {},
	"migrateReset": function() {},
	"attrFn": {
	},
	"uaMatch": function() {},
	"browser": {
		"chrome": function() {},
		"version": function() {},
		"webkit": function() {}
	},
	"sub": function() {},
	"clean": function() {},
	"maskElement": function() {},
	"unmaskElement": function() {},
	"ui": {
		"version": function() {},
		"keyCode": {
			"BACKSPACE": function() {},
			"COMMA": function() {},
			"DELETE": function() {},
			"DOWN": function() {},
			"END": function() {},
			"ENTER": function() {},
			"ESCAPE": function() {},
			"HOME": function() {},
			"LEFT": function() {},
			"NUMPAD_ADD": function() {},
			"NUMPAD_DECIMAL": function() {},
			"NUMPAD_DIVIDE": function() {},
			"NUMPAD_ENTER": function() {},
			"NUMPAD_MULTIPLY": function() {},
			"NUMPAD_SUBTRACT": function() {},
			"PAGE_DOWN": function() {},
			"PAGE_UP": function() {},
			"PERIOD": function() {},
			"RIGHT": function() {},
			"SPACE": function() {},
			"TAB": function() {},
			"UP": function() {}
		},
		"ie": function() {},
		"plugin": {
			"add": function() {},
			"call": function() {}
		},
		"hasScroll": function() {},
		"mouse": function() {},
		"draggable": function() {},
		"droppable": function() {},
		"intersect": function() {},
		"ddmanager": {
			"droppables": {
				"default": {
				}
			},
			"prepareOffsets": function() {},
			"drop": function() {},
			"dragStart": function() {},
			"drag": function() {},
			"dragStop": function() {}
		},
		"resizable": function() {},
		"selectable": function() {},
		"sortable": function() {},
		"accordion": function() {},
		"autocomplete": function() {},
		"button": function() {},
		"buttonset": function() {},
		"datepicker": {
			"version": function() {}
		},
		"dialog": function() {},
		"menu": function() {},
		"position": {
			"fit": {
				"left": function() {},
				"top": function() {}
			},
			"flip": {
				"left": function() {},
				"top": function() {}
			},
			"flipfit": {
				"left": function() {},
				"top": function() {}
			}
		},
		"progressbar": function() {},
		"slider": function() {},
		"spinner": function() {},
		"tabs": function() {},
		"tooltip": function() {}
	},
	"widget": function() {},
	"Widget": function() {},
	"effects": {
		"effect": {
			"blind": function() {},
			"bounce": function() {},
			"clip": function() {},
			"drop": function() {},
			"explode": function() {},
			"fade": function() {},
			"fold": function() {},
			"highlight": function() {},
			"pulsate": function() {},
			"puff": function() {},
			"scale": function() {},
			"size": function() {},
			"shake": function() {},
			"slide": function() {},
			"transfer": function() {}
		},
		"animateClass": function() {},
		"version": function() {},
		"save": function() {},
		"restore": function() {},
		"setMode": function() {},
		"getBaseline": function() {},
		"createWrapper": function() {},
		"removeWrapper": function() {},
		"setTransition": function() {}
	},
	"Color": function() {},
	"datepicker": {
		"_keyEvent": function() {},
		"_disabledInputs": {
		},
		"_datepickerShowing": function() {},
		"_inDialog": function() {},
		"_mainDivId": function() {},
		"_inlineClass": function() {},
		"_appendClass": function() {},
		"_triggerClass": function() {},
		"_dialogClass": function() {},
		"_disableClass": function() {},
		"_unselectableClass": function() {},
		"_currentClass": function() {},
		"_dayOverClass": function() {},
		"regional": {
			"": {
				"closeText": function() {},
				"prevText": function() {},
				"nextText": function() {},
				"currentText": function() {},
				"monthNames": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"monthNamesShort": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"dayNames": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {}
				},
				"dayNamesShort": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {}
				},
				"dayNamesMin": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {}
				},
				"weekHeader": function() {},
				"dateFormat": function() {},
				"firstDay": function() {},
				"isRTL": function() {},
				"showMonthAfterYear": function() {},
				"yearSuffix": function() {}
			},
			"fr": {
				"closeText": function() {},
				"prevText": function() {},
				"nextText": function() {},
				"currentText": function() {},
				"monthNames": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"monthNamesShort": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {},
					"7": function() {},
					"8": function() {},
					"9": function() {},
					"10": function() {},
					"11": function() {}
				},
				"dayNames": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {}
				},
				"dayNamesShort": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {}
				},
				"dayNamesMin": {
					"0": function() {},
					"1": function() {},
					"2": function() {},
					"3": function() {},
					"4": function() {},
					"5": function() {},
					"6": function() {}
				},
				"weekHeader": function() {},
				"dateFormat": function() {},
				"firstDay": function() {},
				"isRTL": function() {},
				"showMonthAfterYear": function() {},
				"yearSuffix": function() {}
			}
		},
		"_defaults": {
			"showOn": function() {},
			"showAnim": function() {},
			"showOptions": {
			},
			"appendText": function() {},
			"buttonText": function() {},
			"buttonImage": function() {},
			"buttonImageOnly": function() {},
			"hideIfNoPrevNext": function() {},
			"navigationAsDateFormat": function() {},
			"gotoCurrent": function() {},
			"changeMonth": function() {},
			"changeYear": function() {},
			"yearRange": function() {},
			"showOtherMonths": function() {},
			"selectOtherMonths": function() {},
			"showWeek": function() {},
			"calculateWeek": function() {},
			"shortYearCutoff": function() {},
			"duration": function() {},
			"numberOfMonths": function() {},
			"showCurrentAtPos": function() {},
			"stepMonths": function() {},
			"stepBigMonths": function() {},
			"altField": function() {},
			"altFormat": function() {},
			"constrainInput": function() {},
			"showButtonPanel": function() {},
			"autoSize": function() {},
			"disabled": function() {},
			"closeText": function() {},
			"prevText": function() {},
			"nextText": function() {},
			"currentText": function() {},
			"monthNames": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {},
				"4": function() {},
				"5": function() {},
				"6": function() {},
				"7": function() {},
				"8": function() {},
				"9": function() {},
				"10": function() {},
				"11": function() {}
			},
			"monthNamesShort": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {},
				"4": function() {},
				"5": function() {},
				"6": function() {},
				"7": function() {},
				"8": function() {},
				"9": function() {},
				"10": function() {},
				"11": function() {}
			},
			"dayNames": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {},
				"4": function() {},
				"5": function() {},
				"6": function() {}
			},
			"dayNamesShort": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {},
				"4": function() {},
				"5": function() {},
				"6": function() {}
			},
			"dayNamesMin": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {},
				"4": function() {},
				"5": function() {},
				"6": function() {}
			},
			"weekHeader": function() {},
			"dateFormat": function() {},
			"firstDay": function() {},
			"isRTL": function() {},
			"showMonthAfterYear": function() {},
			"yearSuffix": function() {}
		},
		"dpDiv": function() {},
		"initialized": function() {},
		"uuid": function() {},
		"version": function() {},
		"markerClassName": function() {},
		"maxRows": function() {},
		"_widgetDatepicker": function() {},
		"setDefaults": function() {},
		"_attachDatepicker": function() {},
		"_newInst": function() {},
		"_connectDatepicker": function() {},
		"_attachments": function() {},
		"_autoSize": function() {},
		"_inlineDatepicker": function() {},
		"_dialogDatepicker": function() {},
		"_destroyDatepicker": function() {},
		"_enableDatepicker": function() {},
		"_disableDatepicker": function() {},
		"_isDisabledDatepicker": function() {},
		"_getInst": function() {},
		"_optionDatepicker": function() {},
		"_changeDatepicker": function() {},
		"_refreshDatepicker": function() {},
		"_setDateDatepicker": function() {},
		"_getDateDatepicker": function() {},
		"_doKeyDown": function() {},
		"_doKeyPress": function() {},
		"_doKeyUp": function() {},
		"_showDatepicker": function() {},
		"_updateDatepicker": function() {},
		"_shouldFocusInput": function() {},
		"_checkOffset": function() {},
		"_findPos": function() {},
		"_hideDatepicker": function() {},
		"_tidyDialog": function() {},
		"_checkExternalClick": function() {},
		"_adjustDate": function() {},
		"_gotoToday": function() {},
		"_selectMonthYear": function() {},
		"_selectDay": function() {},
		"_clearDate": function() {},
		"_selectDate": function() {},
		"_updateAlternate": function() {},
		"noWeekends": function() {},
		"iso8601Week": function() {},
		"parseDate": function() {},
		"ATOM": function() {},
		"COOKIE": function() {},
		"ISO_8601": function() {},
		"RFC_822": function() {},
		"RFC_850": function() {},
		"RFC_1036": function() {},
		"RFC_1123": function() {},
		"RFC_2822": function() {},
		"RSS": function() {},
		"TICKS": function() {},
		"TIMESTAMP": function() {},
		"W3C": function() {},
		"_ticksTo1970": function() {},
		"formatDate": function() {},
		"_possibleChars": function() {},
		"_get": function() {},
		"_setDateFromField": function() {},
		"_getDefaultDate": function() {},
		"_determineDate": function() {},
		"_daylightSavingAdjust": function() {},
		"_setDate": function() {},
		"_getDate": function() {},
		"_attachHandlers": function() {},
		"_generateHTML": function() {},
		"_generateMonthYearHeader": function() {},
		"_adjustInstDate": function() {},
		"_restrictMinMax": function() {},
		"_notifyChange": function() {},
		"_getNumberOfMonths": function() {},
		"_getMinMaxDate": function() {},
		"_getDaysInMonth": function() {},
		"_getFirstDayOfMonth": function() {},
		"_canAdjustMonth": function() {},
		"_isInRange": function() {},
		"_getFormatConfig": function() {},
		"_formatDate": function() {}
	},
	"position": {
		"scrollbarWidth": function() {},
		"getScrollInfo": function() {},
		"getWithinInfo": function() {}
	},
	"cookie": function() {},
	"removeCookie": function() {},
	"bbq": {
		"pushState": function() {},
		"getState": function() {},
		"removeState": function() {}
	},
	"deparam": function() {},
	"elemUrlAttr": function() {},
	"hashchangeDelay": function() {},
	"scrollTo": function() {},
	"window": function() {},
	"Window": {
		"getInstance": function() {},
		"getVersion": function() {},
		"prepare": function() {},
		"closeAll": function() {},
		"hideAll": function() {},
		"showAll": function() {},
		"getAll": function() {},
		"getWindow": function() {},
		"getSelectedWindow": function() {},
		"_iconOnLoad": function() {}
	},
	"tablesorter": {
		"defaults": {
			"cssHeader": function() {},
			"cssAsc": function() {},
			"cssDesc": function() {},
			"cssChildRow": function() {},
			"sortInitialOrder": function() {},
			"sortMultiSortKey": function() {},
			"sortLocaleCompare": function() {},
			"textExtraction": function() {},
			"parsers": {
			},
			"widgets": {
			},
			"widgetZebra": {
				"css": {
					"0": function() {},
					"1": function() {}
				}
			},
			"headers": {
			},
			"widthFixed": function() {},
			"cancelSelection": function() {},
			"sortList": {
			},
			"headerList": {
			},
			"dateFormat": function() {},
			"decimal": function() {},
			"selectorHeaders": function() {},
			"debug": function() {}
		},
		"benchmark": function() {},
		"construct": function() {},
		"addParser": function() {},
		"addWidget": function() {},
		"formatFloat": function() {},
		"formatInt": function() {},
		"isDigit": function() {},
		"clearTableBody": function() {}
	}
};
var butor = {
	"Class": function() {},
	"EventMgr": function() {},
	"Timer": function() {},
	"Logger": function() {},
	"Loader": function() {},
	"ga": {
		"activate": function() {},
		"push": function() {},
		"enable": function() {},
		"disable": function() {}
	},
	"PopoverTooltip": function() {},
	"Utils": {
		"leftPad": function() {},
		"formatDateTime": function() {},
		"formatDate": function() {},
		"formatTime": function() {},
		"parseDate": function() {},
		"getUUID": function() {},
		"isEmpty": function() {},
		"nullToEmpty": function() {},
		"isInteger": function() {},
		"isFloat": function() {},
		"enable": function() {},
		"disable": function() {},
		"selectNavItem": function() {},
		"fillSelect": function() {},
		"formatCurrency": function() {},
		"formatInt": function() {},
		"formatYield": function() {},
		"formatPrice": function() {},
		"formatNumber": function() {},
		"multiColSort": function() {}
	},
	"DatePicker": function() {},
	"AttrSet": function() {},
	"Bundle": function() {},
	"AJAX": {
		"setApp": function() {},
		"call": function() {},
		"streamingReq": function() {},
		"onNotification": function() {},
		"fakeResponse": function() {},
		"maskFieldsInRequestLog": function() {}
	},
	"App": function() {},
	"dlg": {
		"Dialog": function() {}
	},
	"TableCtxtMenu": function() {},
	"Grid": function() {},
	"RowEditor": function() {},
	"CellEditor": function() {},
	"FloatCellEditor": function() {},
	"panel": {
		"ProgressPanel": function() {}
	},
	"MsgPanel": function() {},
	"BottomPanel": function() {},
	"minify": {
		"TABS": function() {},
		"genExterns": function() {},
		"externAll": function() {}
	},
	"LiveSearch": function() {},
	"PortalApp": function() {}
};
var Upload = {
};
var App = {
	"_user": {
		"creationTime": function() {},
		"lastAccessTime": function() {},
		"ssoId": function() {},
		"firstName": function() {},
		"lastName": function() {},
		"id": function() {},
		"displayName": function() {},
		"firmId": function() {},
		"twoStepsSignin": function() {},
		"theme": function() {},
		"firmName": function() {},
		"profile": {
			"_props": {
				"portal.default.page": function() {},
				"language": function() {}
			},
			"_provider": {
				"load": function() {},
				"update": function() {},
				"remove": function() {}
			},
			"base": function() {},
			"__butor_cls_fields__": {
			},
			"construct": function() {},
			"setProvider": function() {},
			"load": function() {},
			"set": function() {},
			"save": function() {},
			"get": function() {},
			"remove": function() {},
			"logDebug": function() {},
			"logInfo": function() {},
			"logError": function() {},
			"logWarn": function() {},
			"msgInfo": function() {},
			"msgWarning": function() {},
			"msgError": function() {},
			"tr": function() {},
			"bind": function() {},
			"unbind": function() {},
			"unbindAll": function() {},
			"fire": function() {},
			"constructor": function() {},
			"__butor_cls_name__": function() {},
			"getClassName": function() {}
		},
		"funcs": {
			"portal.support": function() {},
			"sec.audit": function() {},
			"sec.auths": function() {},
			"sec.firms": function() {},
			"sec.funcs": function() {},
			"sec.groups": function() {},
			"sec.roles": function() {},
			"sec.sec": function() {},
			"sec.users": function() {}
		}
	},
	"_curPage": {
		"pageId": function() {},
		"pageDef": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {},
			"pageId": function() {}
		},
		"appId": function() {},
		"appDef": {
			"title": {
				"en": function() {},
				"fr": function() {}
			}
		},
		"sys": function() {}
	},
	"_loadingPage": {
		"pageId": function() {},
		"pageDef": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {},
			"pageId": function() {}
		},
		"appId": function() {},
		"appDef": {
			"title": {
				"en": function() {},
				"fr": function() {}
			}
		},
		"sys": function() {}
	},
	"_appsDef": {
		"portal": {
			"title": {
				"en": function() {},
				"fr": function() {}
			}
		},
		"sec": {
			"title": {
				"en": function() {},
				"fr": function() {}
			},
			"navBar": function() {},
			"modules": {
				"0": function() {}
			}
		},
		"ao": {
			"title": {
				"en": function() {},
				"fr": function() {}
			},
			"navBar": function() {},
			"modules": {
				"0": function() {},
				"1": function() {},
				"2": function() {}
			}
		},
		"ips": {
			"title": {
				"en": function() {},
				"fr": function() {}
			},
			"navBar": function() {},
			"modules": {
				"0": function() {},
				"1": function() {},
				"2": function() {},
				"3": function() {}
			}
		}
	},
	"_pagesDef": {
		"welcome": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"portal.support": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {},
			"pageId": function() {}
		},
		"ao.ao": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ao.newApp": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ao.app": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ao.pdfToImage": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ao.rnd": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ao.sign": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ips.clientList": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ips.newApp": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ips.managers": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ips.app": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"ips.history": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.start": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.firms": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.users": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.groups": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.funcs": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.roles": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.auths": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"sec.audit": {
			"sys": function() {},
			"secFunc": function() {},
			"url": function() {},
			"title": function() {}
		},
		"portal.contactUs": {
			"sys": function() {},
			"url": function() {},
			"title": function() {}
		}
	},
	"_menuDef": {
		"0": {
			"icon": function() {},
			"page": function() {}
		},
		"1": {
			"icon": function() {},
			"page": function() {}
		},
		"2": {
			"icon": function() {},
			"page": function() {}
		},
		"3": {
			"icon": function() {},
			"caption": function() {},
			"page": function() {}
		},
		"4": {
			"icon": function() {},
			"caption": {
				"en": function() {},
				"fr": function() {}
			},
			"secFunc": {
				"sys": function() {},
				"func": function() {}
			},
			"func": function() {}
		},
		"5": {
			"icon": function() {},
			"caption": function() {},
			"secFunc": {
				"sys": function() {},
				"func": function() {}
			},
			"func": function() {}
		}
	},
	"_firstPage": function() {},
	"_defaultPage": function() {},
	"_appHeader": function() {},
	"base": function() {},
	"Bundle": {
		"add": function() {},
		"get": function() {},
		"override": function() {}
	},
	"isEmpty": function() {},
	"formatDate": function() {},
	"parseDate": function() {},
	"isInteger": function() {},
	"isFloat": function() {},
	"events": {
		"bind": function() {},
		"fire": function() {},
		"unbind": function() {}
	},
	"Utils": {
		"escapeStr": function() {},
		"escape": function() {},
		"leftPad": function() {},
		"formatDateTime": function() {},
		"formatDate": function() {},
		"formatTime": function() {},
		"parseDate": function() {},
		"getUUID": function() {},
		"isEmpty": function() {},
		"nullToEmpty": function() {},
		"isInteger": function() {},
		"isFloat": function() {},
		"enable": function() {},
		"disable": function() {},
		"selectNavItem": function() {},
		"fillSelect": function() {},
		"formatCurrency": function() {},
		"formatInt": function() {},
		"formatYield": function() {},
		"formatPrice": function() {},
		"formatNumber": function() {},
		"multiColSort": function() {}
	},
	"AttrSet": {
		"base": function() {},
		"__butor_cls_fields__": {
		},
		"getCodeSet": function() {},
		"getCodeSets": function() {},
		"createHelper": function() {},
		"logDebug": function() {},
		"logInfo": function() {},
		"logError": function() {},
		"logWarn": function() {},
		"msgInfo": function() {},
		"msgWarning": function() {},
		"msgError": function() {},
		"tr": function() {},
		"bind": function() {},
		"unbind": function() {},
		"unbindAll": function() {},
		"fire": function() {},
		"constructor": function() {},
		"__butor_cls_name__": function() {},
		"getClassName": function() {}
	},
	"notif": {
		"_opened": function() {},
		"_timer": {
			"_fnc": function() {},
			"_delay": function() {},
			"base": function() {},
			"__butor_cls_fields__": {
			},
			"construct": function() {},
			"stop": function() {},
			"start": function() {},
			"restart": function() {},
			"logDebug": function() {},
			"logInfo": function() {},
			"logError": function() {},
			"logWarn": function() {},
			"msgInfo": function() {},
			"msgWarning": function() {},
			"msgError": function() {},
			"tr": function() {},
			"bind": function() {},
			"unbind": function() {},
			"unbindAll": function() {},
			"fire": function() {},
			"constructor": function() {},
			"__butor_cls_name__": function() {},
			"getClassName": function() {}
		},
		"_subscriptions": {
		},
		"base": function() {},
		"__butor_cls_fields__": {
			"_opened": function() {},
			"_subscriptions": {
			}
		},
		"construct": function() {},
		"_getWsUrl": function() {},
		"start": function() {},
		"send": function() {},
		"stop": function() {},
		"_checkSanity": function() {},
		"subscribe": function() {},
		"unsubscribe": function() {},
		"onMessage": function() {},
		"logDebug": function() {},
		"logInfo": function() {},
		"logError": function() {},
		"logWarn": function() {},
		"msgInfo": function() {},
		"msgWarning": function() {},
		"msgError": function() {},
		"tr": function() {},
		"bind": function() {},
		"unbind": function() {},
		"unbindAll": function() {},
		"fire": function() {},
		"constructor": function() {},
		"__butor_cls_name__": function() {},
		"getClassName": function() {}
	},
	"_themeName": function() {},
	"_sessionId": function() {},
	"_msgPanel": {
		"_parent": function() {},
		"_appId": function() {},
		"_panel": function() {},
		"_close": function() {},
		"_sign": function() {},
		"_strong": function() {},
		"_msg": function() {},
		"_errorDelay": function() {},
		"_delay": function() {},
		"base": function() {},
		"__butor_cls_fields__": {
			"_errorDelay": function() {},
			"_delay": function() {}
		},
		"construct": function() {},
		"show": function() {},
		"setPosition": function() {},
		"setDelay": function() {},
		"setErrorDelay": function() {},
		"hideMsg": function() {},
		"showMsg": function() {},
		"hide": function() {},
		"_prepData": function() {},
		"info": function() {},
		"warning": function() {},
		"error": function() {},
		"tr": function() {},
		"getPanel": function() {},
		"logDebug": function() {},
		"logInfo": function() {},
		"logError": function() {},
		"logWarn": function() {},
		"msgInfo": function() {},
		"msgWarning": function() {},
		"msgError": function() {},
		"bind": function() {},
		"unbind": function() {},
		"unbindAll": function() {},
		"fire": function() {},
		"constructor": function() {},
		"__butor_cls_name__": function() {},
		"getClassName": function() {}
	},
	"_sd": function() {},
	"_pl": function() {},
	"_eventMgr": {
		"_events": {
			"setupDone": {
				"0": function() {}
			},
			"profileLoaded": {
				"0": function() {}
			},
			"historyChanged": {
				"0": function() {}
			},
			"langChanged": {
				"0": function() {}
			}
		},
		"base": function() {},
		"__butor_cls_fields__": {
		},
		"construct": function() {},
		"bind": function() {},
		"unbind": function() {},
		"unbindAll": function() {},
		"fire": function() {},
		"constructor": function() {},
		"__butor_cls_name__": function() {},
		"getClassName": function() {}
	},
	"dlg": {
		"registry": {
		},
		"getDlg": function() {},
		"openDlg": function() {}
	},
	"_env": function() {},
	"_options": {
	},
	"__butor_cls_fields__": {
		"_appsDef": {
		},
		"_pagesDef": {
		},
		"_menuDef": {
		},
		"_defaultPage": function() {}
	},
	"construct": function() {},
	"start": function() {},
	"_loadTheme": function() {},
	"_resized": function() {},
	"_langChanged": function() {},
	"_setDefaultPage": function() {},
	"_goHistory": function() {},
	"_detachPage": function() {},
	"sessionTimedOut": function() {},
	"_buildMainMenu": function() {},
	"_reorgMenu": function() {},
	"_createMenuHandler": function() {},
	"_loadAppModules": function() {},
	"_showPageStep2": function() {},
	"setTitle": function() {},
	"_showPageBody": function() {},
	"_setup": function() {},
	"_loadUserInfo": function() {},
	"_signInOut": function() {},
	"_changePwd": function() {},
	"_setQRsPwd": function() {},
	"getId": function() {},
	"isUserKnown": function() {},
	"isUserAdmin": function() {},
	"getUser": function() {},
	"getUserProfile": function() {},
	"hasAccess": function() {},
	"goHome": function() {},
	"showPage": function() {},
	"getCurPageDef": function() {},
	"getPageArgs": function() {},
	"registerCleanup": function() {},
	"setLang": function() {},
	"switchLangPortal": function() {},
	"ping": function() {},
	"_contactUs": function() {},
	"mailTo": function() {},
	"getMsgPanel": function() {},
	"bind": function() {},
	"unbind": function() {},
	"unbindAll": function() {},
	"fire": function() {},
	"constructor": function() {},
	"__butor_cls_name__": function() {},
	"getClassName": function() {},
	"setOptions": function() {},
	"addBundle": function() {},
	"getLang": function() {},
	"pushHistory": function() {},
	"changeState": function() {},
	"getState": function() {},
	"getEnv": function() {},
	"themeLoaded": function() {},
	"getThemeName": function() {},
	"createMsgPanel": function() {},
	"createBottomPanel": function() {},
	"createBackToTopLink": function() {},
	"mask": function() {},
	"unmask": function() {},
	"getSessionId": function() {},
	"setSessionId": function() {},
	"scrollTo": function() {},
	"showStatus": function() {},
	"progress": function() {},
	"removeStatus": function() {},
	"showMsg": function() {},
	"hideMsg": function() {},
	"error": function() {},
	"info": function() {},
	"warning": function() {},
	"switchLang": function() {},
	"translate": function() {},
	"translateElem": function() {},
	"tr": function() {},
	"logDebug": function() {},
	"logInfo": function() {},
	"logError": function() {},
	"logWarn": function() {},
	"msgInfo": function() {},
	"msgWarning": function() {},
	"msgError": function() {}
};
var AJAX = {
	"setApp": function() {},
	"call": function() {},
	"streamingReq": function() {},
	"onNotification": function() {},
	"fakeResponse": function() {},
	"maskFieldsInRequestLog": function() {}
};
var LOGGER = {
	"_level": function() {},
	"base": function() {},
	"__butor_cls_fields__": {
		"_level": function() {}
	},
	"construct": function() {},
	"setLevel": function() {},
	"infoEnabled": function() {},
	"warnEnabled": function() {},
	"errorEnabled": function() {},
	"debugEnabled": function() {},
	"traceEnabled": function() {},
	"_replaceArgs": function() {},
	"info": function() {},
	"warn": function() {},
	"error": function() {},
	"debug": function() {},
	"trace": function() {},
	"logDebug": function() {},
	"logInfo": function() {},
	"logError": function() {},
	"logWarn": function() {},
	"msgInfo": function() {},
	"msgWarning": function() {},
	"msgError": function() {},
	"tr": function() {},
	"bind": function() {},
	"unbind": function() {},
	"unbindAll": function() {},
	"fire": function() {},
	"constructor": function() {},
	"__butor_cls_name__": function() {},
	"getClassName": function() {}
};

