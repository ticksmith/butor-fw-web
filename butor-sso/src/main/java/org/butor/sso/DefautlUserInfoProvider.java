/**
 * Copyright 2013-2017 butor.com
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.butor.sso;

import org.butor.auth.common.firm.Firm;
import org.butor.auth.common.firm.FirmServices;
import org.butor.auth.common.user.User;
import org.butor.auth.common.user.UserKey;
import org.butor.auth.common.user.UserQuestions;
import org.butor.auth.common.user.UserServices;
import org.butor.json.service.Context;
import org.butor.json.service.ResponseHandlerHelper;
import org.butor.json.util.ContextBuilder;
import org.butor.json.util.JsonResponse;
import org.butor.utils.AccessMode;

public class DefautlUserInfoProvider implements UserInfoProvider {
	private String authUsername;
	private UserServices userServices = null;
	private FirmServices firmServices = null;

	public DefautlUserInfoProvider() {
		super();
	}


	@Override
	public User readUser(String id, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<User> handler = ResponseHandlerHelper.createJsonResponse(User.class);
		Context<User> ctx = new ContextBuilder<User>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		userServices.readUser(ctx, id, null);
		return handler.getRow();
	}


	@Override
	public Firm readFirm(long firmId, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<Firm> handler = ResponseHandlerHelper.createJsonResponse(Firm.class);
		Context<Firm> ctx = new ContextBuilder<Firm>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		firmServices.readFirm(ctx, firmId, "sec", "firms", AccessMode.READ);
		return handler.getRow();
	}

	@Override
	public UserKey insertUser(User user, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<UserKey> handler = ResponseHandlerHelper.createJsonResponse(UserKey.class);
		Context<UserKey> ctx = new ContextBuilder<UserKey>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		userServices.insertUser(ctx, user);
		return handler.getRow();
	}

	@Override
	public UserKey updateUser(User user, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<UserKey> handler = ResponseHandlerHelper.createJsonResponse(UserKey.class);
		Context<UserKey> ctx = new ContextBuilder<UserKey>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		userServices.updateUser(ctx, user);
		return handler.getRow();
	}

	@Override
	public UserKey updateState(User user, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<UserKey> handler = ResponseHandlerHelper.createJsonResponse(UserKey.class);
		Context<UserKey> ctx = new ContextBuilder<UserKey>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		userServices.updateState(ctx, user);
		return handler.getRow();
	}
	@Override
	public UserQuestions readUserQuestions(String id, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<UserQuestions> handler = ResponseHandlerHelper.createJsonResponse(UserQuestions.class);
		Context<UserQuestions> ctx = new ContextBuilder<UserQuestions>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		userServices.readQuestions(ctx, id);
		return handler.getRow();
	}
	@Override
	public UserKey updateQuestions(UserQuestions qrs, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<UserKey> handler = ResponseHandlerHelper.createJsonResponse(UserKey.class);
		Context<UserKey> ctx = new ContextBuilder<UserKey>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		userServices.updateQuestions(ctx, qrs);
		return handler.getRow();
	}

	@Override
	public String resetLogin(String id, String url, boolean resetAndSendLink, String sessionId, String reqId, String lang, String domain) {
		JsonResponse<String> handler = ResponseHandlerHelper.createJsonResponse(String.class);
		Context<String> ctx = new ContextBuilder<String>()
				.createCommonRequestArgs(authUsername, lang, sessionId, reqId, domain)
				.setResponseHandler(handler)
				.build();
		userServices.resetLogin(ctx, id, url, resetAndSendLink);
		return handler.getRow();
	}
	
	public void setAuthUsername(String insertUserUsername) {
		this.authUsername = insertUserUsername;
	}


	public void setUserServices(UserServices userServices) {
		this.userServices = userServices;
	}


	public void setFirmServices(FirmServices firmServices) {
		this.firmServices = firmServices;
	}
}
